**This project has been discontinued - switch from Drone + Gitlab CI to Netlify**

# Monitoring all my websites 

This is a project that gives me the ability to monitor my website, using phpservermon

Don't forget to add the .env file in the root of the folder, in order to have a `MYSQL_PASSWORD` set for the database 👍